"""NCU 1062 Otakuology Final Project"""

# Python build-in
import math
import random
import sys
# Third party
import pygame

# Environment settings
pygame.init()
WINDOW_WIDTH, WINDOW_HEIGHT = 1280, 720
SCREEN = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT))
CLOCK = pygame.time.Clock()
# Load picture
ORIGIN_PIC_PLAYER_L_ = pygame.image.load('pic/hifumi_l.png')
ORIGIN_PIC_PLAYER_R = pygame.image.load('pic/hifumi_r.png')
ORIGIN_PIC_ENEMY = pygame.image.load('pic/soujiro.png')
ORIGIN_PIC_THORN = pygame.image.load('pic/thorn.png')
# Picture resize
PIC_PLAYER_L = pygame.transform.scale(ORIGIN_PIC_PLAYER_L_, (50, 168))
PIC_PLAYER_R = pygame.transform.scale(ORIGIN_PIC_PLAYER_R, (50, 168))
PIC_ENEMY = pygame.transform.scale(ORIGIN_PIC_ENEMY, (110, 110))
PIC_THORN = pygame.transform.scale(ORIGIN_PIC_THORN, (50, 10))

class Background(pygame.sprite.Sprite):
    def __init__(self, image_file):
        pygame.sprite.Sprite.__init__(self)
        pic = pygame.image.load(image_file)
        self.image = pygame.transform.scale(pic, (WINDOW_WIDTH, WINDOW_HEIGHT))
        self.rect = self.image.get_rect()
        self.rect.left, self.rect.top = 0, 0

class Player(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.hp = 1
        # Load image and set its location
        self.image = PIC_PLAYER_R
        self.rect = self.image.get_rect()
        self.rect.x = 50
        self.rect.y = (WINDOW_HEIGHT - self.rect.width) // 2
    def update(self, key_pressed, move_offset):
        new_x, new_y = self.rect.x, self.rect.y
        if key_pressed[pygame.K_LEFT]:
            self.image = PIC_PLAYER_L
            if new_x - move_offset >= 0:
                new_x -= move_offset
        if key_pressed[pygame.K_RIGHT]:
            self.image = PIC_PLAYER_R
            if new_x + move_offset + self.rect.width <= WINDOW_WIDTH:
                new_x += move_offset
        if key_pressed[pygame.K_UP]:
            if new_y - move_offset >= 0:
                new_y -= move_offset
        if key_pressed[pygame.K_DOWN]:
            if new_y + move_offset + self.rect.height <= WINDOW_HEIGHT:
                new_y += move_offset
        # Set new position
        self.rect.x, self.rect.y = new_x, new_y
        # Put image on the rectangle
        SCREEN.blit(self.image, self.rect)
    def got_damage(self):
        self.hp -= 1
        if self.hp == 0:
            # Be killed
            return True
        return False

class Enemy(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.alive = True
        # Load image and set its location
        self.image = PIC_ENEMY
        self.rect = self.image.get_rect()
        self.rect.x = 1000
        self.rect.y = (WINDOW_HEIGHT - self.rect.width) // 2
        self.vector = [0, 0]
    def update(self, need_random):
        if need_random:
            self.vector = [random.randint(0, 14) - 7, random.randint(0, 14) - 7]
        new_x, new_y = self.rect.x + self.vector[0], self.rect.y + self.vector[1]
        # Check if it's in screen
        if new_x >= 0 and new_x + self.rect.width <= WINDOW_WIDTH:
            self.rect.x = new_x
        if new_y >= 0 and new_y + self.rect.height <= WINDOW_HEIGHT:
            self.rect.y = new_y
        # Put image on the rectangle
        SCREEN.blit(self.image, self.rect)
    def is_alive(self):
        return self.alive

class Thorn(pygame.sprite.Sprite):
    def __init__(self, location, move_offset):
        pygame.sprite.Sprite.__init__(self)
        self.alive = True
        degree = random.randint(0, 360)
        self.vector_x = int(math.cos(degree/180 * math.pi) * move_offset)
        self.vector_y = int(math.sin(degree/180 * math.pi) * move_offset)
        # Load image and set its location
        self.image = pygame.transform.rotate(PIC_THORN, -degree)
        self.rect = self.image.get_rect()
        self.rect.x, self.rect.y = location
    def update(self):
        self.rect.x += self.vector_x
        self.rect.y += self.vector_y
        # Put image on the rectangle
        SCREEN.blit(self.image, self.rect)
        # Kill if out of the screen
        if self.rect.x < -self.rect.width or self.rect.x > WINDOW_WIDTH:
            self.alive = False
        if self.rect.y < -self.rect.height or self.rect.y > WINDOW_HEIGHT:
            self.alive = False
    def is_alive(self):
        return self.alive

def game():
#####################
# Game start settings
#####################
    in_start = True
    color_off = (10, 10, 10)
    color_on = (124, 24, 24)
    start_background = Background('pic/start.png')
    font = pygame.font.Font('pic/consola.ttf', 70)
#####################
# Game start
#####################
    while in_start:
        # Paint background first
        SCREEN.blit(start_background.image, start_background.rect)
        # Color change and click while mouse on or not
        start_color = exit_color = color_off
        mouse_x, mouse_y = pygame.mouse.get_pos()
        if mouse_x > 1020 - 133 and mouse_x < 1020 + 133 and mouse_y > 370 and mouse_y < 430:
            start_color = color_on
            for event in pygame.event.get():
                if event.type == pygame.MOUSEBUTTONUP:
                    in_start = False
                    break
                if event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                    sys.exit(0)
                if event.type == pygame.KEYDOWN and event.key == pygame.K_RETURN:
                    in_start = False
                    break
        if mouse_x > 1020 - 50 and mouse_x < 1020 + 50 and mouse_y > 470 and mouse_y < 530:
            exit_color = color_on
            for event in pygame.event.get():
                if event.type == pygame.MOUSEBUTTONUP:
                    sys.exit(0)
                if event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                    sys.exit(0)
                if event.type == pygame.KEYDOWN and event.key == pygame.K_RETURN:
                    in_start = False
                    break
        start = font.render('Game Start', True, start_color)
        over = font.render('Exit', True, exit_color)
        SCREEN.blit(start, (1020 - start.get_width() // 2, 400 - start.get_height() // 2))
        SCREEN.blit(over, (1020 - over.get_width() // 2, 500 - over.get_height() // 2))
        # For once
        for event in pygame.event.get():
            # Press [x] button or [esc] key to quit game
            if event.type == pygame.QUIT:
                sys.exit(0)
            if event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                sys.exit(0)
            if event.type == pygame.KEYDOWN and event.key == pygame.K_RETURN:
                in_start = False
                break
        # Screen repaint
        pygame.display.flip()
        CLOCK.tick(120)

#####################
# Main loop settings
#####################
    thorn_list = []
    thorn_counter = 0
    enemy_counter = 0
    is_done = False
    is_catch = False
    player = Player()
    enemy = Enemy()
    game_background = Background('pic/background.jpg')
#####################
# Main loop in 60 FPS
#####################
    while not is_done:
        # Paint background first
        SCREEN.blit(game_background.image, game_background.rect)
        # For once
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit(0)
            if event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                is_done = True
            if event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
                is_done = player.got_damage()
            if event.type == pygame.KEYDOWN and event.key == pygame.K_RETURN:
                is_done = True
                is_catch = True
        # Create thorn
        if thorn_counter == 0 or thorn_counter >= 5:
            thorn_list.append(Thorn((enemy.rect.x + 20, enemy.rect.y + 20), 5))
            thorn_counter = 0
        thorn_counter += 1
        # Move each thorn
        index = 0
        while index < len(thorn_list):
            thorn_list[index].update()
            if not thorn_list[index].is_alive():
                del thorn_list[index]
            else:
                index += 1
        # Move enemy
        if enemy_counter == 0 or enemy_counter >= 60:
            enemy.update(True)
            enemy_counter = 0
        else:
            enemy.update(False)
        enemy_counter += 1
        # Move player
        pressed = pygame.key.get_pressed()
        player.update(pressed, 10)
        # Collision detect
        if sum([player.rect.colliderect(thorn) for thorn in thorn_list]) >= 1:
            is_done = True
        if player.rect.colliderect(enemy):
            is_done = True
            is_catch = True
        # Screen repaint
        pygame.display.flip()
        CLOCK.tick(120)

#####################
# End of game
#####################
    in_end = True
    if random.randint(0, 99) % 2 == 0:
        str_success = 'pic/success_1.png'
    else:
        str_success = 'pic/success_2.jpg'
    while in_end:
        if is_catch:
            end_background = Background(str_success)
        else:
            end_background = Background('pic/failed.png')
        SCREEN.blit(end_background.image, end_background.rect)
        # Press any key to quit
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit(0)
            if event.type == pygame.KEYDOWN and event.key == pygame.K_RETURN:
                in_end = False
                break
        # Screen repaint
        pygame.display.flip()
        CLOCK.tick(120)

if __name__ == '__main__':
    while True:
        game()
